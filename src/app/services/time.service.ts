import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Session } from '../models/Session';
import { environment } from 'src/environments/environment';
import { SessionService } from './session.service';
import { UserService } from './user.service';

@Injectable({
  providedIn: 'root'
})
export class TimeService {
  constructor(private httpClient: HttpClient, private userService: UserService, private sessionService: SessionService) {}

  deleteTime(time_id) {    
    this.httpClient.delete(environment.apiUrl + 'time/delete/' + time_id, {
      headers: { Authorization: 'Basic ' + this.userService.getUser().token }
    }).subscribe(data => {
      let session = new Session(data);
      session.times = this.sessionService.serializeTimes(session.times);
      this.sessionService.setCurrentSession(session);
    });
  }

  addTime(duration,scramble) {       
    this.httpClient.post(environment.apiUrl + 'time/add/', {
      session_id: this.sessionService.getCurrentSession().value.id,
      duration: duration,
      scramble: scramble
    },{
      headers: { Authorization: 'Basic ' + this.userService.getUser().token }
    }).subscribe(data => {
      let session = new Session(data);
      session.times = this.sessionService.serializeTimes(session.times);
      this.sessionService.setCurrentSession(session);
    });
  }

  addPenalty(time_id,penalty) {         
    this.httpClient.post(environment.apiUrl + 'time/addPenalty/', {time_id, penalty},{
      headers: { Authorization: 'Basic ' + this.userService.getUser().token }
    }).subscribe(data => {      
      let session = new Session(data);
      session.times = this.sessionService.serializeTimes(session.times);
      this.sessionService.setCurrentSession(session);
    });
  }
}
