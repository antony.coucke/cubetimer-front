import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { UserService } from './user.service';

@Injectable({
  providedIn: 'root'
})
export class ScrambleService {
  private currentScramble = new BehaviorSubject([]);

  constructor(private httpClient:HttpClient, private userService: UserService) {
    this.nextScramble();
  }

  nextScramble() {
    this.httpClient.get(environment.apiUrl + 'scramble/get/',{
      headers: { Authorization: 'Basic ' + this.userService.getUser().token }
    }).subscribe((scramble:string[]) => {
      this.currentScramble.next(scramble);
    });
  }

  getScramble() {
    return this.currentScramble;
  }
}
