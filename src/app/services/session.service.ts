import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Session } from '../models/Session';
import { Time } from '../models/Time';
import { BehaviorSubject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { UserService } from './user.service';

@Injectable({
  providedIn: 'root'
})
export class SessionService {
  private sessions: BehaviorSubject<Session[]> = new BehaviorSubject<Session[]>([]);
  private currentSession: BehaviorSubject<Session> = new BehaviorSubject<Session>(null);

  constructor(private httpClient: HttpClient, private userService: UserService) {
  }

  createSession() {
    return this.httpClient.post<Session>(environment.apiUrl + 'session/add', {}, {
      headers: { Authorization: 'Basic ' + this.userService.getUser().token }
    });
  }

  deleteSession(session_id: number) {        
    return this.httpClient.delete<Session[]>(environment.apiUrl + 'session/delete/' + session_id, {
      headers: { Authorization: 'Basic ' + this.userService.getUser().token }
    }).subscribe(sessions => {
      this.sessions.next(this.serializeSessions(sessions));
    });
  }

  async fetchSessions() {   
    this.sessions.next(null); 
    this.setCurrentSession(null);    
    await this.httpClient.get<Session[]>(environment.apiUrl + 'session/get', {
      headers: { Authorization: 'Basic ' + this.userService.getUser().token }
    }).toPromise().then(sessions => {
      console.log(sessions)
      this.sessions.next(this.serializeSessions(sessions));
    });
  }

  getSessions() {
    this.fetchSessions();    
    return this.sessions;
  }

  async getSession(session_id: number) {
    await this.fetchSessions();
    for(var session of this.sessions.value) {
      if(session.id === session_id) {
        this.currentSession.next(session);
      }
    }
  }

  getCurrentSession() {
    return this.currentSession;
  }

  setCurrentSession(session) {
    this.currentSession.next(session);
  }

  serializeSessions(data: any[]) {
    let serialized = [];
    for(var session of data) {
      session.times = this.serializeTimes(session.times);
      serialized.push(new Session(session));
    }
    return serialized;
  }

  serializeTimes(data: any[]) {
    let serialized = [];    
    for(var time of data) {
      serialized.push(new Time(time));
    }
    return serialized;
  }
}
