import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { User } from '../models/User';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private user = new BehaviorSubject(new User());

  constructor(private httpClient: HttpClient) { 
    this.checkIfUserIsLogged();
  }

  checkIfUserIsLogged() {
    if(localStorage.getItem('credentials') !== null) {
      let data = JSON.parse(localStorage.getItem('credentials'));
      let user = new User(data);
      this.user.next(user);
    }
  }

  getUser() {
    return this.user.value;
  }

  register(data) {
    return this.httpClient.post(environment.apiUrl + 'user/register/', {
      email: data.email,
      username: data.username,
      password: data.password,
      passwordConfirm: data.passwordConfirm
    });
  }

  login(data) {  
    return this.httpClient.post(environment.apiUrl + 'user/login/', {}, {
      headers: { Authorization: 'Basic ' + btoa(data.username + ":" + data.password) }
    });
  }

  logout() {
    localStorage.removeItem('credentials');
  }

  setCredentialsInLocalStorage(data) {
    let user = new User();
    user.email = data.email;
    user.username = data.username;
    user.token = btoa(data.username + ":" + data.password);
    user.isLogged = true;    
    this.user.next(user);
    localStorage.setItem('credentials',JSON.stringify(user));
  }
}
