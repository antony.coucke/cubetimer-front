import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { CustomError } from 'src/app/models/Error'
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ErrorService {
  private errorSource = new BehaviorSubject(null);
  error = this.errorSource.asObservable();

  constructor(private httpClient:HttpClient) {
    this.testApi(); //Permet de tester la connection à l'API
  }

  addError(error: CustomError) {
    this.errorSource.next(error);
  }

  private testApi() {
    this.httpClient.get(environment.apiUrl + 'testAPI').subscribe(response => {}, errorResponse => {
      const error = new CustomError(errorResponse.message,errorResponse.status,true);
      this.addError(error);
    })
  }

}
