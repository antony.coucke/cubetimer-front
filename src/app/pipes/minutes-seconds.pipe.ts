import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'minutesSeconds'
})
export class MinutesSecondsPipe implements PipeTransform {
  transform(value: number): string {
    if (value >= 60) {
      const minutes: number = Math.floor(value / 60);
      return minutes.toString().padStart(2, '0') +
        ':' + (value - minutes * 60 < 10 ? '0' : '') +
        (value - minutes * 60).toFixed(2).padStart(2, '0');
    }
    return value.toString();
  }
}
