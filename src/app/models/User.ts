export class User {
    private _email: string;
    private _username: string;
    private _token: string;
    private _logged:boolean = false;

    constructor(data=null) {
        if(data !== null) {
            this.email = data?._email;
            this.username = data?._username;
            this.token = data?._token;
            this.isLogged = data?._logged;
        }
    }

    //Getters
    set email(email:string) {
        this._email = email;
    }
    set username(username:string) {
        this._username = username;
    }
    set token(token:string) {
        this._token = token;
    }
    set isLogged(isLogged:boolean) {
        this._logged = isLogged;
    }    

    //Setters
    get email(): string {
        return this._email;
    }
    get username(): string {
        return this._username;
    }
    get token(): string {
        return this._token;
    }
    get isLogged(): boolean {
        return this._logged;
    }
}