import { Time } from './Time';

export class Session {
    private _times:Time[];
    private _id:number;
    private _best:number;
    private _worst:number;
    private _average:number;
    private _mo3:number;
    private _ao5:number;
    private _ao10:number;
    private _ao30:number;
    private _ao50:number;
    private _ao100:number;
    private _created_at:string;
    private _updated_at:string;

    constructor(data=null) {
        if(data !== null) {
            this.id = data?.id;
            this.best = data?.best;
            this.worst = data?.worst;
            this.average = data?.average;
            this.mo3 = data?.mo3;
            this.ao5 = data?.ao5;
            this.ao10 = data?.ao10;
            this.ao30 = data?.ao30;
            this.ao50 = data?.ao50;
            this.ao100 = data?.ao100;
            this.times = data?.times;
            this.created_at = data?.created_at;
            this.updated_at = data?.updated_at;
        }
    }
    
    //Getters
	public get times(): Time[] {
		this._times.sort((a:Time,b:Time) =>  {
			return b.id - a.id
		})
		let j = this._times.length;
		for(var i in this._times) {
			this._times[i].key = j;
			j--;
		}
		return this._times;
	}
	public get id(): number {
		return this._id;
	}
	public get best(): number {
		return this._best;
	}
	public get worst(): number {
		return this._worst;
	}
	public get average(): number {
		return this._average;
	}
	public get mo3(): number {
		return this._mo3;
	}
	public get ao5(): number {
		return this._ao5;
	}
	public get ao10(): number {
		return this._ao10;
	}
	public get ao30(): number {
		return this._ao30;
	}
	public get ao50(): number {
		return this._ao50;
	}
	public get ao100(): number {
		return this._ao100;
    }
	public get created_at(): string {
		return this._created_at;
    }
	public get updated_at(): string {
		return this._updated_at;
    }
    
    //Setters
	public set times(value: Time[]) {
		this._times = value;
	}
	public set id(value: number) {
		this._id = value;
	}
	public set best(value: number) {
		this._best = value;
	}
	public set worst(value: number) {
		this._worst = value;
	}
	public set average(value: number) {
		this._average = value;
	}
	public set mo3(value: number) {
		this._mo3 = value;
	}
	public set ao5(value: number) {
		this._ao5 = value;
	}
	public set ao10(value: number) {
		this._ao10 = value;
	}
	public set ao30(value: number) {
		this._ao30 = value;
	}
	public set ao50(value: number) {
		this._ao50 = value;
	}
	public set ao100(value: number) {
		this._ao100 = value;
	}    
	public set updated_at(value: string) {
		this._updated_at = value;
	}   
	public set created_at(value: string) {
		this._created_at = value;
	}   
}