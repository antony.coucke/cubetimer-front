import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

export class CustomError {
    message:string;
    status:string;
    isFatal:boolean;

    constructor(message,status,isFatal) {
        this.message = message;
        this.status = status;
        this.isFatal = isFatal;
    }
}