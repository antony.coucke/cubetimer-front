export class Time {    
    private _id:number;
    private _duration:number;
	private _scramble:number;
	private _key:number;
    private _created_at:string;
    private _updated_at:string;

    constructor(data=null) {        
        if(data !== null) {
            this.id = data?.id;
            this.duration = data?.duration;
            this.scramble = data?.scramble;
            this.created_at = data?.created_at;
            this.updated_at = data?.updated_at;
        }
    }    

    //Getters
	public get id(): number {
		return this._id;
	}
	public get duration(): number {
		return this._duration;
	}
	public get scramble(): number {
		return this._scramble;
	}
	public get key(): number {
		return this._key;
	}
	public get created_at(): string {
		return this._created_at;
	}
	public get updated_at(): string {
		return this._updated_at;
    }   
     
    //Setters
	public set id(value: number) {
		this._id = value;
	}
	public set duration(value: number) {
		this._duration = value;
	}
	public set scramble(value: number) {
		this._scramble = value;
	}
	public set key(value: number) {
		this._key = value;
	}
	public set created_at(value: string) {
		this._created_at = value;
	}
	public set updated_at(value: string) {
		this._updated_at = value;
	}
}