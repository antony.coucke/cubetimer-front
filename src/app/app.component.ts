import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ErrorComponent } from './components/common/error/error.component';
import { CustomError } from './models/Error';
import { ErrorService } from './services/error.service';
import { UserService } from './services/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'cubetimer-front-new';
  error:CustomError = null;
  
  constructor(private errorService:ErrorService, private userService: UserService,private modalService:NgbModal, private router : Router) {
    this.handleRootRedirection();
    this.handleErrors();
  }

  handleRootRedirection() {
    if(this.router.url === '/') {
      this.userService.getUser().isLogged ? this.router.navigate(['session/list']) : this.router.navigate(['auth'])
    }
  }

  handleErrors() {
    this.errorService.error.subscribe(error => { 
      if(error !== null) {   
        const modalRef = this.modalService.open(ErrorComponent, {size: 'lg', backdrop: 'static', keyboard: false});
        modalRef.componentInstance.error = error;
        this.error = error;
      }
    })
  }
}
