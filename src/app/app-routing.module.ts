import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SessionGetComponent } from './components/session/session-get/session-get.component';
import { SessionListComponent } from './components/session/session-list/session-list.component';
import { AuthComponent } from './components/user/auth/auth.component';
import { IsLoggedGuard } from './guards/is-logged.guard';
import { IsNotLoggedGuard } from './guards/is-not-logged.guard';
import { UserService } from './services/user.service';

const routes: Routes = [
  { path: 'auth', component: AuthComponent, canActivate: [IsNotLoggedGuard] },
  { path: 'session/list', component: SessionListComponent, canActivate: [IsLoggedGuard] },
  { path: 'session/get/:session_id', component: SessionGetComponent, canActivate: [IsLoggedGuard] },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
