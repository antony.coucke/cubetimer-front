import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { CustomError } from 'src/app/models/Error';

@Component({
  selector: 'app-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.scss']
})
export class ErrorComponent implements OnInit {
  @Input() error:CustomError;

  constructor(public activeModal: NgbActiveModal, private router:Router) {}

  ngOnInit(): void {
  }

  refreshPage() {
    return window.location.reload();
  }

  goToHome() {
    this.activeModal.close();
    this.router.navigate(['/']);
  }
}
