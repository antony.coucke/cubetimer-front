import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { faPlus, faUser } from '@fortawesome/free-solid-svg-icons';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  faUser=faUser;
  faPlus=faPlus;
  logged:boolean;
  navbarOpen:boolean = false;

  constructor(private userService: UserService, private router: Router) {
    this.logged = this.userService.getUser().isLogged;
  }

  ngOnInit(): void {
  }  

  toggleNavbar() {
    this.navbarOpen = !this.navbarOpen;
  }

  logout() {
    this.userService.logout();
    this.router.navigate([''])
    window.location.reload();
  }

}
