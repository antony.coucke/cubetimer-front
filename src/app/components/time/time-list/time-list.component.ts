import { KeyValue, Time } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-time-list',
  templateUrl: './time-list.component.html',
  styleUrls: ['./time-list.component.scss']
})
export class TimeListComponent implements OnInit {
  @Input() session;
  times:Time[] = [];

  constructor() {}

  ngOnInit(): void {}
}
