import { Component, Input, OnInit } from '@angular/core';
import { faTimes } from '@fortawesome/free-solid-svg-icons';
import { TimeService } from 'src/app/services/time.service';

@Component({
  selector: '[app-time-list-row]',
  templateUrl: './time-list-row.component.html',
  styleUrls: ['./time-list-row.component.scss']
})
export class TimeListRowComponent implements OnInit {
  @Input() time;
  faTimes=faTimes;

  constructor(private timeService: TimeService) {}

  deleteTime() {
    this.timeService.deleteTime(this.time.id);
  }

  addPenalty(penalty) {
    this.timeService.addPenalty(this.time.id, penalty);
  }

  ngOnInit(): void {}
}
