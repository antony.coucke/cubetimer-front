import { Component, HostListener, OnInit } from '@angular/core';
import { ScrambleService } from 'src/app/services/scramble.service';
import { TimeService } from 'src/app/services/time.service';

@Component({
  selector: 'app-timer',
  templateUrl: './timer.component.html',
  styleUrls: ['./timer.component.scss']
})
export class TimerComponent implements OnInit {

  count: number = 0;
  counting:boolean = false;  
  currentTime:number = 0;
  timerRef;

  constructor(private timeService:TimeService, private scrambleService: ScrambleService) {
  }

  @HostListener('document:keyup', ['$event'])
  handleKeyboardEvent(event: KeyboardEvent) {
    if(event.key==' ' && event.type=='keyup' ) {
      !this.counting ? this.startCount() : this.stopCount();
    } 
  }

  ngOnInit(): void {
  }

  startCount(): void {
    this.counting = true;
    const startTime = Date.now() - (this.count || 0);
    this.timerRef = setInterval(() => {
      this.count = (Date.now() - startTime)/1000;
    });
  }

  stopCount(): void {
    this.counting = false;
    this.currentTime = this.count;
    this.timeService.addTime(this.currentTime,this.scrambleService.getScramble().value.join(' '));
    this.resetCount();
    this.scrambleService.nextScramble();
  }

  resetCount() {
    clearInterval(this.timerRef);
    this.count = 0;
  }

}
