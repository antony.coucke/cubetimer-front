import { Component, OnInit } from '@angular/core';
import { Session } from '../../../models/Session';
import { Observable } from 'rxjs';
import { SessionService } from 'src/app/services/session.service';
import { faCircleNotch, faPlus, faSpinner } from '@fortawesome/free-solid-svg-icons';
import { Router } from '@angular/router';

@Component({
  selector: 'app-session-list',
  templateUrl: './session-list.component.html',
  styleUrls: ['./session-list.component.scss']
})
export class SessionListComponent implements OnInit {
  sessions:Observable<Session[]> = null;
  faSpinner=faCircleNotch;
  faPlus=faPlus;

  constructor(private sessionService: SessionService, private router: Router) {
  }

  newSession() {
    this.sessionService.createSession().subscribe(session => {
      this.router.navigate(['/session/get/', session.id])
    })
  }

  ngOnInit(): void {    
    this.sessions = this.sessionService.getSessions().asObservable();
  }
}
