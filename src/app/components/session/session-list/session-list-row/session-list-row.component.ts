import { Component, Input, OnInit } from '@angular/core';
import { faChartBar, faPen, faTimes } from '@fortawesome/free-solid-svg-icons';
import { Session } from '../../../../models/Session';
import { SessionService } from 'src/app/services/session.service';

@Component({
  selector: '[app-session-list-row]',
  templateUrl: './session-list-row.component.html',
  styleUrls: ['./session-list-row.component.scss']
})
export class SessionListRowComponent implements OnInit {
  @Input() session: Session;
  faTimes=faTimes;
  faPen=faPen;
  faChartBar=faChartBar;

  constructor(private sessionService: SessionService) { 
  }

  deleteSession(session_id) {
    this.sessionService.deleteSession(session_id);
  }
  
  ngOnInit(): void {}
}
