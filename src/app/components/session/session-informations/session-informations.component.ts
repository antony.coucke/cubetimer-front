import { Component, Input, OnInit } from '@angular/core';
import { Session } from '../../../models/Session';
import { SessionService } from 'src/app/services/session.service';

@Component({
  selector: 'app-session-informations',
  templateUrl: './session-informations.component.html',
  styleUrls: ['./session-informations.component.scss']
})
export class SessionInformationsComponent implements OnInit {
  @Input() session;
  constructor() {}

  ngOnInit(): void {}

}
