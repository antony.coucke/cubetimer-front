import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Session } from '../../../models/Session';
import { BehaviorSubject, Observable } from 'rxjs';
import { SessionService } from 'src/app/services/session.service';

@Component({
  selector: 'app-session-get',
  templateUrl: './session-get.component.html',
  styleUrls: ['./session-get.component.scss']
})
export class SessionGetComponent implements OnInit {
  session: Observable<Session>;

  constructor(private sessionService: SessionService, private route: ActivatedRoute) {     
    this.session = this.sessionService.getCurrentSession().asObservable();
    this.route.params.subscribe(params => { 
      this.sessionService.getSession(parseInt(params.session_id));
    });
  }

  ngOnInit(): void {}

  ngOnDestroy() {
    const currentSession = this.sessionService.getCurrentSession().value;
    if(currentSession.times.length === 0)
      this.sessionService.deleteSession(currentSession.id); 

  }
}
