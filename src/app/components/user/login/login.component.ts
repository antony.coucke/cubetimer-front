import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from 'src/app/models/User';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm;
  registered:boolean = false;
  errors:any = {};

  constructor(private formBuilder: FormBuilder, private userService: UserService, private router: Router) {
    this.loginForm = this.formBuilder.group({ username: '', password: ''});
  }
 
  login(data) {
    this.userService.login(data).subscribe((user:User) => {
      data.email = user.email;
      this.userService.setCredentialsInLocalStorage(data);
      this.router.navigate([''])
      window.location.reload();
    }, errors => {      
      for(var error of errors.error) {
        this.errors[error.field] = error.message;
      }
    })
  }

  ngOnInit(): void {}
}
