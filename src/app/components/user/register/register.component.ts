import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  registerForm;
  registered:boolean = false;
  errors:any = {};

  constructor(private formBuilder: FormBuilder, private userService: UserService) {
    this.registerForm = this.formBuilder.group({ email: '', username: '', password: '', passwordConfirm: '' });
  }

  register(data) {
    this.userService.register(data).subscribe(response => {
      this.registered = true;
    }, errors => {      
      for(var error of errors.error) {
        this.errors[error.field] = error.message;
      }
    })
  }

  ngOnInit(): void {}
}
