import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ScrambleService } from 'src/app/services/scramble.service';

@Component({
  selector: 'app-current-scramble',
  templateUrl: './current-scramble.component.html',
  styleUrls: ['./current-scramble.component.scss']
})
export class CurrentScrambleComponent implements OnInit {
  scramble:Observable<string[]>;

  constructor(private scrambleService: ScrambleService) { 
    this.scramble = this.scrambleService.getScramble();
  }

  ngOnInit(): void {}

}
