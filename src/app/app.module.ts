import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/common/navbar/navbar.component';
import { HttpClientModule } from '@angular/common/http';
import { ErrorComponent } from './components/common/error/error.component'
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RegisterComponent } from './components/user/register/register.component';
import { LoginComponent } from './components/user/login/login.component';
import { AuthComponent } from './components/user/auth/auth.component';
import { SessionListComponent } from './components/session/session-list/session-list.component';
import { SessionListRowComponent } from './components/session/session-list/session-list-row/session-list-row.component';
import { TimeListComponent } from './components/time/time-list/time-list.component';
import { TimeListRowComponent } from './components/time/time-list/time-list-row/time-list-row.component';
import { SessionGetComponent } from './components/session/session-get/session-get.component';
import { TimerComponent } from './components/time/timer/timer.component';


import { MinutesSecondsPipe } from './pipes/minutes-seconds.pipe';
import { CurrentScrambleComponent } from './components/scramble/current-scramble/current-scramble.component';
import { SessionInformationsComponent } from './components/session/session-informations/session-informations.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    ErrorComponent,
    RegisterComponent,
    LoginComponent,
    AuthComponent,
    SessionListComponent,
    SessionListRowComponent,
    TimeListComponent,
    TimeListRowComponent,
    SessionGetComponent,
    TimerComponent,
    MinutesSecondsPipe,
    CurrentScrambleComponent,
    SessionInformationsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FontAwesomeModule,
    FormsModule,ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
